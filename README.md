
# My React Youtube

Welcome to my react youtube app

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

**Knowledge Prerequisites:**

1. Knowledge of `HTML`, `CSS` & `Javascript`
2. Knowledge of [SCSS](https://sass-lang.com/) (**NOT** SASS)
4. Knowledge of `React, Redux`

**Software Installed Prerequisites:**

1. [NPM](https://www.npmjs.com/get-npm)
2. React

### Installing 
Please follow the steps below **IN ORDER**:

1. yarn install
2. yarn start
